# 中期报告

## 1. 项目信息

项目名称：API 模块 UT 测试问题解决

项目编号：210290131

项目成员：

- 项目主导师：Kejia Chen
- 学生：Yuanhao Ji

项目仓库：[gitlab.summer-ospp.ac.cn/summer2021/210290131](https://gitlab.summer-ospp.ac.cn/summer2021/210290131)

项目描述：

- 问题：在部分 UT 中使用 PowerMock 后，导致 sonar 不能获取对应的测试报告。
- 大概原因：PowerMock 模拟 JDK 的静态方法、构造方法、final 方法、私有方法时，需要把使用这些方法的类加入到 `@PrepareForTest` 注解中，从而导致单元测试覆盖率不被统计。 对此问题进行排查，并给出解决方案。

目前已 merge 的 PR：

- [[Fix-5781][UT] Fix test coverage in sonar](https://github.com/apache/dolphinscheduler/pull/5817)
- [[Improvement-5921][CI] Improve maven connection in CI builds](https://github.com/apache/dolphinscheduler/pull/5924)

## 2. 方案描述

### 2.1 问题定位

1. 问题复现

   如下图所示，许多 UT 的覆盖率统计出现错误。

   ![](img/1.png)

   DolphinScheduer 使用 `PowerMock` 单元测试模拟框架，通过 `JaCoCo` 统计覆盖率指标后由 `Sonar` 存储和管理。

   ![](img/2.jpg)

   因此，为了分析问题出现的原因，首先必须要使问题复现。

   在 fork 仓库后，可以在编写测试代码后，通过提交 PR 的方式触发 UT 的工作流，然后检查 `Sonar` 中的覆盖率是否正确。

2. 分析问题

   目前，可以猜测这个问题与 `PowerMock` 和 `JaCoCo` 有关，`PowerMock` 模拟 JDK 的静态方法、构造方法、final 方法、私有方法时，需要把使用这些方法的类加入到 `@PrepareForTest` 注解中，从而导致 `JaCoCo` 统计的单元测试覆盖率出现错误。 如以下例子：

   ```java
   public class Demo {
   
       public int method() {
           // do something
           return -1;
       }
   
   }
   ```

   ```java
   @RunWith(PowerMockRunner.class)
   @PrepareForTest({Demo.class})
   public class DemoTest {
   
       @Mock
       private Demo demo;
   
       @Test
       public void testMethod() {
           when(demo.method()).thenReturn(1);
           Assert.assertEquals(1, demo.method());
       }
   
   }
   ```

    在实际测试中，`Demo.class` 的 `method()` 方法的覆盖率为 0

### 2.2 可能的解决方案

经过调研，如果这个问题是由 `PowerMock` 和 `JaCoCo` 引起的话，可以通过 `JaCoCo` 的 `offline instrumentation` 模式解决。

参考：

- [Jacoco's offline instrumentation](https://github.com/powermock/powermock/wiki/Code-coverage-with-JaCoCo#offline-instrumentation)

- [JaCoCo statistics PowerMock coverage is zero solution](https://www.programmersought.com/article/68501226854/)

- [@PrepareForTest包含的类Jacoco报告中覆盖率为0](https://segmentfault.com/a/1190000022182279)

- [JaCoCo Offline模式](http://www.tianjuzi.com/article/jacoco_5)

### 2.3 编写单元测试模板

最后，编写一份单元测试模板，明确编写测试代码时应遵守的标准。单元测试模板应包含以下几个方面：

- 如何编写标准的测试代码？
- 在编写测试代码时应考虑哪些方面？
- 如何避免使用 `PowerMock`？
- 如何编写易测试代码？
- 如何分析单元测试评估结果？

## 3. 时间规划

|            日期            |                             工作                             |
| :------------------------: | :----------------------------------------------------------: |
|     第一周：7-1 ~ 7-4      | 熟悉 DolphinScheduler API 的 UT，了解 `PowerMock` 和 `Jacoco` |
|   第二、三周：7-5 ~ 7-18   | 搜集资料，理解 `PowerMock` 和 `Jacoco` 的实现原理，复现问题  |
|    第四周：7-19 ~ 7-25     |          定位问题后，编写一份报告描述问题出现的原因          |
|   第五、六周：7-26 ~ 8-8   |                           修复问题                           |
|     第七周：8-9 ~ 8-15     |                修复问题后，编写并提交中期报告                |
|    第八周：8-16 ~ 8-22     |                      总结单元测试的要点                      |
|   第九、十周：8-23 ~ 9-5   |        编写单元测试模板，并与 mentor 讨论形成最终文档        |
| 第十一至十三周：9-6 ~ 9-26 |                    缓冲时间（为社区贡献）                    |
|   第十四周：9-27 ~ 9-30    |            撰写评估文件，提交研发成果进行最终评估            |

## 4. 项目进度

### 4.1 已完成工作

#### 4.1.1 问题定位

经过分析和实践，确定了该问题出现的原因是—— `PowerMock` 和 `JaCoCo` 在类加载时修改字节码冲突导致的。

1. 明确问题出处，缩小定位范围。

   DolphinScheduler UT 中使用了 `PowerMock` 作为单元测试模拟框架，由 `JaCoCo` 统计各项覆盖率指标，最后由 `Sonar` 存储和管理覆盖率数据。而问题表现为 —— `Sonar` 中的某些 UT 覆盖率为 0，但是 `Sonar` 只是一个代码质量管理平台，覆盖率数值并不是由它得出的，`Sonar` 只是读取了 `JaCoCo` 生成的测试报告。那么，很容易看出，这个问题一定与 `JaCoCo` 脱不了干系。

   进一步观察发现，并非所有 UT 的覆盖率都有问题，只用到 `JUnit` 的 UT 就可以正常获取覆盖率，而所有出现覆盖率问题的 UT 都少不了 `PowerMock` 的影子。

   因此，可以得出结论——覆盖率问题一定与 `PowerMock` 和 `JaCoCo` 有关。

2. 搜集资料，寻找相似问题。

   分析问题之后，我们首先可以借助搜索引擎找到与之相似或相同的问题，通过了解这些问题的背景和解决方案也许可以找到我们问题的关键点。

   基于以上分析，可以以 `JaCoCo`、`PowerMock`、`覆盖率` 这三个关键字在 Google、Baidu 等搜索引擎上搜索资料。幸运的是，网上有大量同类问题，并且都与 `JaCoCo`、`PowerMock` 有关。

   其中，正如 [Code-coverage-with-JaCoCo](https://github.com/powermock/powermock/wiki/Code-coverage-with-JaCoCo) 中所说 —— there is NO WAY TO USE PowerMock with JaCoCo On-the-fly instrumentation。`PowerMock` 和 `JaCoCo` 的 `On-the-fly instrumentation` 之间存在问题。经验证发现，DolphinScheduler 目前正是使用的这两者结合的方式，因此这应该就是问题的关键所在。

3. 理解问题出现原因。

   基于搜索结果和官方文档，深入理解问题出现的原因。

   首先，如 [JaCoCo Class Ids](https://www.eclemma.org/jacoco/trunk/doc/classids.html) 中的描述，`JaCoCo` 基于 CRC64 算法计算得到 64 位的 Class Id，用它来标识 Java 类，在运行时为每个加载的类采样执行数据并存储到 `*.exec` 文件中，在分析时（例如生成报告），Class Id 用于将分析的类与执行数据相关联。而出现覆盖率为 0% 的原因，就是因为待测试的类与 `JaCoCo` 实际采样的类不一致，通过 CRC64 算法计算的 Class Id 不相同，从而导致待测试类的覆盖率始终为 0。

   那么，为何会出现这种情况呢？上述页面中也给出了答复 —— 这种情况通常发生在配置 JaCoCo 代理之前配置了另一个 Java Agent，或者特殊的类加载器预处理了这些类文件，例如：Mocking frameworks、Application servers、Persistence frameworks。而 `PowerMock` 就是一种 Mock 框架，进一步了解其原理得知，`JaCoCo` 在加载 class 时会事先把统计代码插入到 class 中（即插桩），当执行到某个测试类时，`PowerMock` 检测到该类使用了`@PrepareForTest` 注解，就会在加载该类时使用 `Javassist` 直接从 class 文件中重新读取字节码，从而导致丢失了 `JaCoCo` 的修改，使得 `JaCoCo` 实际统计的 class 是被修改之后的，而被修改之后的 class 中并没有 `JaCoCo` 所插入的统计代码，同时 class 文件发生变化，Class Id 与加载时不一致，在分析时，执行数据无法与待测试类相关联，最终导致覆盖率始终为 0。

    如下图所示，Demo 类后面还跟有 `MockitoMock` 的字样，说明这是一个由 `PowerMock` 模拟的对象，而 `JaCoCo` 却错把它以为是最开始时所加载的 Demo 类。

   ![](img/4.png)

4. 制定解决方案。

   `JaCoCo` 的 `offline instrumentation` 可以解决此类问题，它的原理是这样的：在测试前先对 class 文件进行插桩，然后生成插过桩的 class 文件或 jar 包，测试插过桩的 class 和 jar 包后，生成动态覆盖信息到文件，最后统一对覆盖信息进行处理，并生成报告。

   `On-the-fly` 模式更方便简单进行代码覆盖分析，无需提前进行字节码插桩，无需考虑 classpath 的设置。

   但存在如下情况则不适合 `On-the-fly`，需要采用 `offline` 提前对字节码插桩：

   （1）运行环境不支持 java agent。

   （2）部署环境不允许设置 JVM 参数。

   （3）字节码需要被转换成其他的虚拟机如 Android Dalvik VM。

   （4）动态修改字节码过程中和其他 agent 冲突。

   （5）无法自定义用户加载类。

   而目前问题就是因为动态修改字节码过程中与 `PowerMock` 冲突，因此必须使用 `offline` 模式。

#### 4.1.2 问题修复

目前已经通过将 `JaCoCo` 切换为 `offline instrumentation` 模式解决了问题，PR: [#5817](https://github.com/apache/dolphinscheduler/pull/5817)

1. 实现方案

   使用 `JaCoCo` `offline instrumentation` 模式替代 `on-the-fly` 模式。`offline instrumentation` 模式可以在测试前先将统计代码（即“插桩”）插入到 class 字节码文件中，基于插过桩的 class 字节码文件进行测试，在测试过程中，对于 `@PrepareForTest` 注解所标记的测试类，`PowerMock` 在加载该类时重新读取的字节码是 `JaCoCo` 修改以后的（在 `on-the-fly` 模式中 `PowerMock` 读取的是原来的字节码，因此会丢失 `JaCoCo` 的修改），所以依然保留了 `JaCoCo` 对其所做的更改，避免了修改冲突，因此 `JaCoCo` 可以正常统计覆盖率相关指标，生成正确的测试报告。

   `offline instrumentation` 大致思路：

   - 生成插桩的 class 文件（对应插件中的 `instrument`）
   - 插过桩的 class 文件进行测试（对应于 `mvn test`）
   - 恢复 class 文件（对应插件中的 `restore-instrumented-classes`）
   - 生成报告（对应插件中的 `report`）

   `JaCoCo` 的 `on-the-fly` 模式和 `offline instrumentation` 模式是通过在 `jacoco-maven-plugin` 插件中执行不同的 `execution` 实现的。`offline instrumentation` 模式的 `jacoco-maven-plugin` 插件配置如下：

   ```java
   <plugin>
       <groupId>org.jacoco</groupId>
       <artifactId>jacoco-maven-plugin</artifactId>
       <version>${jacoco.version}</version>
       <configuration>
           <dataFile>${project.build.directory}/jacoco.exec</dataFile>
       </configuration>
       <executions>
           <execution>
               <id>default-instrument</id>
               <goals>
                   <goal>instrument</goal>
               </goals>
           </execution>
           <execution>
               <id>default-restore-instrumented-classes</id>
               <goals>
                   <goal>restore-instrumented-classes</goal>
               </goals>
           </execution>
           <execution>
               <id>default-report</id>
               <goals>
                   <goal>report</goal>
               </goals>
           </execution>
       </executions>
   </plugin>
   ```

2. 验证

   使用 `on-the-fly` 模式的报告如下：

   ![](img/8.png)

   使用 `offline instrumentation` 模式的报告如下：

   ![](img/9.png)

   在 `on-the-fly` 模式下的覆盖情况如下：

   ![](img/10.png)

   在 `offline instrumentation` 模式下的覆盖情况如下：

   ![](img/11.png)

   对比可得，使用 `offline instrumentation` 模式后覆盖率由 `46.8%` 提高到了 `48.6%`，覆盖的行数由 12256 行提高到了 12734 行，有较为明显的提升

### 4.2 遇到的问题及解决方案

1. `Sonar` 获取测试报告时报错

   在执行 `mvn verify sonar:sonar` 时，会首先执行 `jacoco-maven-plugin` 插件，过程中报错信息如下：大意是说 `Sonar` 不能处理修改过的 class 字节码文件，猜测是由于 `JaCoCo` 修改 class 字节码文件后没有恢复造成的。

   经过分析，该问题与 Maven 构建生命周期有关。

   （1）首先简单了解一下 Maven 构建生命周期基础知识。

   Maven 构建生命周期定义了一个项目构建、发布的过程。Maven 有以下三个标准的生命周期：

   - **clean**：项目清理的处理
   - **default(或 build)**：项目部署的处理
   - **site**：项目站点文档创建的处理

   其中，**default(或 build)** 是 Maven 的主要生命周期，包括如下 23 个阶段：

   |                生命周期阶段                 |                             描述                             |
   | :-----------------------------------------: | :----------------------------------------------------------: |
   |            **validate**（校验）             |   校验项目是否正确并且所有必要的信息可以完成项目的构建过程   |
   |            initialize（初始化）             |                初始化构建状态，比如设置属性值                |
   |       generate-sources（生成源代码）        |               生成包含在编译阶段中的任何源代码               |
   |        process-sources（处理源代码）        |                  处理源代码，比如过滤任意值                  |
   |     generate-resources（生成资源文件）      |               生成将会包含在项目包中的资源文件               |
   |     process-resources （处理资源文件）      |          复制和处理资源到目标目录，为打包阶段做准备          |
   |             **compile**（编译）             |                       编译项目的源代码                       |
   |        process-classes（处理类文件）        |    处理编译生成的文件，比如对 class 文件做字节码改善优化     |
   |   generate-test-sources（生成测试源代码）   |             生成包含在编译阶段中的任何测试源代码             |
   |   process-test-sources（处理测试源代码）    |                处理测试源代码，比如过滤任意值                |
   | generate-test-resources（生成测试资源文件） |                      为测试创建资源文件                      |
   | process-test-resources（处理测试资源文件）  |                 复制和处理测试资源到目标目录                 |
   |        test-compile（编译测试源码）         |                 编译测试源代码到测试目标目录                 |
   |   process-test-classes（处理测试类文件）    |                  处理测试源码编译生成的文件                  |
   |              **test**（测试）               |          使用合适的单元测试框架运行测试（如 Juint）          |
   |         prepare-package（准备打包）         |       在实际打包之前，执行任何的必要的操作为打包做准备       |
   |             **package**（打包）             | 将编译后的代码打包成可分发格式的文件，比如 JAR、WAR 或者 EAR文件 |
   |     pre-integration-test（集成测试前）      |      在执行集成测试前进行必要的动作。比如搭建需要的环境      |
   |        integration-test（集成测试）         |            处理和部署项目到可以运行集成测试环境中            |
   |     post-integration-test（集成测试后）     |   在执行集成测试完成后进行必要的动作。比如清理集成测试环境   |
   |             **verify** （验证）             |               验证项目包是否有效且达到质量标准               |
   |             **install**（安装）             |  安装项目包到本地仓库，这样项目包可以用作其他本地项目的依赖  |
   |             **deploy**（部署）              |                将最终的项目包发布到远程仓库中                |

   需要注意的是：**当一个阶段通过 Maven 命令调用时，例如 `mvn compile`，该阶段之前以及包括该阶段在内的所有阶段会被执行**。

   `on-the-fly`、`offline instrumentation` 构建过程对比：

   ![](img/7.jpg)

   （2）问题分析

   查看 `jacoco-maven-plugin` 源码得知，`instrument`、`restore-instrumented-classes`、`report` 对应的阶段分别为：`process-classes`、`prepare-package`、`verify`。结合上文得知， `restore-instrumented-classes` 所绑定的 `prepare-package` 阶段在 `test` 之后，而在基于 GitHub Actions 的 ut 工作流中，执行的是 `mvn test`，`restore-instrumented-classes` 并没有执行到，因此也就没有恢复修改的 class 字节码文件。所以，`jacoco-maven-plugin` 至少应该执行 `mvn verify` 才可以执行完全部阶段。

![](img/6.jpg)

### 4.3 后续工作安排

1. 总结单元测试的要点，并形成单元测试模板

2. 撰写评估文件，提交研发成果进行最终评估

