# 1. Project Plan

## 1.1 Positioning problem

1. Reproduce the problem

As shown in the figure below, coverage of some unit tests is incorrect.

![](img/1.png)

As we all know, dolphinscheduler uses `PowerMock` for unit testing, and uses `Jacoco` to count code coverage stored and managed by `Sonar`.

![](img/2.jpg)

Therefore, in order to analyze the cause of the problem, the problem should be reproduced first.

After fork the [repository](https://github.com/apache/dolphinscheduler), we should write some unit test codes, post a PR to trigger
the [workflow](https://github.com/apache/dolphinscheduler/blob/dev/.github/workflows/ci_ut.yml), and then check whether the coverage is correct in Sonar.

2. Analyse the problem

At present, it can be determined that this problem is due to `PowerMock` and `Jacoco`. When using the `@PrepareForTest` annotation only causes code coverage to
fail in some cases. If you want to write test for `Demo.java`, and you add the `Demo.class` in `@PrepareForTest`, then the `Jacoco` will not count the coverage
in `Demo.java`.

```java
public class Demo {

    public int method() {
        // do something
        return -1;
    }

}
```

```java
@RunWith(PowerMockRunner.class)
@PrepareForTest({Demo.class})
public class DemoTest {

    @Mock
    private Demo demo;

    @Test
    public void testMethod() {
        when(demo.method()).thenReturn(1);
        Assert.assertEquals(1, demo.method());
    }

}
```

If you add other class in `@PrepareForTest`, it will not affect the code coverage in Demo.

## 1.2 Possible solution

According to google search results, if it is caused by `PowerMock` and `Jacoco`, we can
use [Jacoco's offline instrumentation](https://github.com/powermock/powermock/wiki/Code-coverage-with-JaCoCo#offline-instrumentation) to solve this problem.

Reference:

- [JaCoCo statistics PowerMock coverage is zero solution](https://www.programmersought.com/article/68501226854/)

- [@PrepareForTest包含的类Jacoco报告中覆盖率为0](https://segmentfault.com/a/1190000022182279)

- [JaCoCo Offline模式](http://www.tianjuzi.com/article/jacoco_5)

## 1.3 Write unit test template

Finally, we should write a unit test template which contains considerations and standards for unit test codes.

I think this template should contain the following.

1. How to write standard unit test code?

2. Considerations when writing unit test code.

3. How avoid using PowerMock?

4. How to write easy-to-test code?

5. Analyze the unit test evaluation results.

# 2. Time plan

|              Date               |                                                    Work                                                    |
| :-----------------------------: | :--------------------------------------------------------------------------------------------------------: |
|        Week 1: 7-1 ~ 7-4        |            Familiar with DolphinScheduler API unit test module, understand PowerMock and Jacoco            |
|   Week 2 ~ Week 3: 7-5 ~ 7-18   |     Collect information, understand the principles of PowerMock and Jacoco, and reproduce the problem      |
|       Week 4: 7-19 ~ 7-25       |                             Write a report explaining the cause of the problem                             |
|   Week 5 ~ Week 6: 7-26 ~ 8-8   |                                              Fix the problem                                               |
|       Week 7: 8-9 ~ 8-15        | Complete problem fixes, write evaluation documents, and submit development results for mid-term evaluation |
|       Week 8: 8-16 ~ 8-22       |                                   Summarize the main points of unit test                                   |
|  Week 9 ~ Week 10: 8-23 ~ 9-5   |        Write a general template for unit testing and discuss with mentor to form the final document        |
| Week 11 ~ Week 13: ：9-6 ~ 9-26 |                                  Buffer (do something for the community)                                   |
|      Week 14: 9-27 ~ 9-30       |                   Write evaluation documents and submit R&D results for final evaluation                   |
