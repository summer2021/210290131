# 单元测试

## 1. 单元测试与集成测试

- 单元测试：

  单元测试（unit testing），是指对软件中的最小可测试单元进行检查和验证。单元测试是在软件开发过程中要进行的最低级别的测试活动，软件的独立单元将在与程序的其他部分相隔离的情况下进行测试

  > 例如，使用 `Junit` 框架，针对某个类写一个对应的测试类，对目标类的大部分主要方法进行测试

- 集成测试：

  集成测试是在单元测试的基础上，将所有模块按照设计要求组装成为子系统或系统，进行集成测试

## 2. 原则

1. 应当针对核心业务代码做精细的单元测试

2. 集成测试时，应当从最顶层开始，自然地覆盖到最底层

3. **模块间不应该相互依赖，而应该依赖于抽象。抽象不应该依赖于具体实现，具体实现应该依赖于抽象。**因此在测试多个类，即集成测试时，可以使用 Mock 手段来模拟所依赖类的实现，以减小耦合

   > 若在编写测试代码过程中，发现需要编写大量模拟代码，那就说明这部分业务代码依赖关系非常复杂，需要反过来优化业务代码

4. 测试应当是秒级甚至是毫秒级的，不应占用过多时间

## 3. Spring Boot 单元测试最佳实践

1. 普通 Bean

   Spring Boot 启动花费时间较长，因此应当避免使用 Spring 进行单元测试，可以通过直接创建目标类对象的方式实现

2. Controller

   通过 Mock 模拟 Http 请求

   以 API 模块为例。所有 Controller 的测试类都继承自 `AbstractControllerTest` 类，在该类的 `setup()` 方法中创建了 `mockMvc` 对象，用于模拟请求，并且该方法声明了 `@Before` 注解，会先于子类执行，可以保证子类可以复用该对象

   ```java
   @RunWith(PowerMockRunner.class)
   @PowerMockRunnerDelegate(SpringRunner.class)
   @SpringBootTest(classes = ApiApplicationServer.class)
   @PrepareForTest({ RegistryCenterUtils.class, RegistryClient.class })
   @PowerMockIgnore({"javax.management.*"})
   public class AbstractControllerTest {
   
       public static final String SESSION_ID = "sessionId";
   
       protected MockMvc mockMvc;
   
       @Autowired
       private WebApplicationContext webApplicationContext;
   
       @Autowired
       private SessionService sessionService;
   
       protected User user;
   
       protected String sessionId;
   
       @Before
       public void setUp() {
           PowerMockito.suppress(PowerMockito.constructor(RegistryClient.class));
           PowerMockito.mockStatic(RegistryCenterUtils.class);
   		
           // 在测试前生成对象，因此子类可以复用该对象
           mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
   
           createSession();
       }
   
       @After
       public void after() throws Exception {
           sessionService.signOut("127.0.0.1", user);
       }
   
       private void createSession() {
   		// do something
       }
   }
   
   ```

   Controller 测试类：

   ```java
   public class LoginControllerTest extends AbstractControllerTest {
   
       private static Logger logger = LoggerFactory.getLogger(LoginControllerTest.class);
   
       @Test
       public void testLogin() throws Exception {
           MultiValueMap<String, String> paramsMap = new LinkedMultiValueMap<>();
           paramsMap.add("userName","cxc");
           paramsMap.add("userPassword","123456");
   		
           // 使用父类的 mockMvc 对象模拟请求
           MvcResult mvcResult = mockMvc.perform(post("/login")
                   .params(paramsMap))
                   .andExpect(status().isOk())
                   .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                   .andReturn();
   
           Result result = JSONUtils.parseObject(mvcResult.getResponse().getContentAsString(), Result.class);
           Assert.assertTrue(result != null && result.isSuccess());
           logger.info(mvcResult.getResponse().getContentAsString());
       }
   }
   
   ```

3. 持久化测试

   相关注解有 `@JdbcTest`、`@DataJpsTest`、`@MybatisTest` 等

   1. 使用内存数据库

      在自动化测试环境中可能需要大量模拟接口，包括数据存储接口，可以通过内存数据库实现，如 `h2database`

   2. 使用外部数据库

      测试持久层时，默认是回滚的。可以在具体的测试方法上添加 `@Rollback(false)` 来禁止回滚，也可以在测试类上添加

   3. 使用 Mock 模拟

      模拟持久化过程，屏蔽持久化实现过程，优点是使测试不依赖外部环境就可以运行，缺点是不适合某些必须进行持久化测试的场景

