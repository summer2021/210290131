# DAO 层测试

## 1. 编写建议

DAO 层测试有三种方案：

1. 使用 mock 对象模拟数据库操作
2. 使用内存数据库进行测试
3. 使用真实数据库环境进行测试

三者对比如下：

- 使用模拟对象有利于提高可测试性，避免环境对单元测试的限制。但是这种方案是建立在数据库操作都正确的假想下，因此并不能完全覆盖所有场景（如对 SQL 语句执行结果的测试）。
- 使用内存数据库进行测试同样避免了环境对单元测试的限制，同时使测试更可靠、更快速。
- 使用真实数据库环境进行测试更具有可靠性，但是也对测试环境有了限制，降低了测试的可迁移性，同时数据库操作可能会使测试时间延长，并产生许多脏数据。 

综上，在涉及数据库的单元测试场景中，推荐使用前两种方案。

## 2. 编写示例

待测试代码：

```java
@Service
public class UsersServiceImpl extends BaseServiceImpl implements UsersService {
    
    /**
     * query user
     *
     * @param name name
     * @param password password
     * @return user info
     */
    @Override
    public User queryUser(String name, String password) {
        String md5 = EncryptionUtils.getMd5(password);
        return userMapper.queryUserByNamePassword(name, md5);
    }
    
}
```

#### 1. mock

> 模拟 DAO 层数据库操作，实现 Service 层测试

测试代码：

```java
@RunWith(MockitoJUnitRunner.class)
public class UsersServiceTest {
    
    private static final Logger logger = LoggerFactory.getLogger(UsersServiceTest.class);
    @InjectMocks
    private UsersServiceImpl usersService;
    @Mock
    private UserMapper userMapper;
    
    @Test
    public void testQueryUser() {
        String userName = "userTest0001";
        String userPassword = "userTest0001";
        when(userMapper.queryUserByNamePassword(userName, EncryptionUtils.getMd5(userPassword))).thenReturn(getGeneralUser());
        User queryUser = usersService.queryUser(userName, userPassword);
        logger.info(queryUser.toString());
        Assert.assertTrue(queryUser != null);
    }
    
    /**
     * get user
     */
    private User getGeneralUser() {
        User user = new User();
        user.setUserType(UserType.GENERAL_USER);
        user.setUserName("userTest0001");
        user.setUserPassword("userTest0001");
        return user;
    }
}
```

#### 2. H2 内存数据库

1. pom 依赖

   ```xml
   <dependency>
       <groupId>com.h2database</groupId>
       <artifactId>h2</artifactId>
       <scope>test</scope>
   </dependency>
   ```

2. 数据库初始化

   > 由于 h2 是内存数据库，不会持久化表结构，因此在每次测试前都要先初始化表结构。
   > 在 `test/resources` 目录下新建 `application.yml` 文件

   ```yaml
   spring:
     database:
     	driver-class-name: org.h2.Driver
   	url: jdbc:h2:mem:test	# test 为数据库名称
       initialization-mode: always	# always: 每次启动时进行初始化
       schema: classpath:sql/schema.sql	# 用于初始化表结构的 sql 文件的路径
       data: classpath:sql/data.sql	# 用于初始化表数据的 sql 文件的路径
   
   # 打印 sql debug 日志
   logging:
     level:
       org.apache.dolphinscheduler.dao.mapper: debug
   ```

3. 测试

   > 基于数据库环境进行测试，无需编写 mock 代码
   
   测试代码：
   
   ```java
   @RunWith(MockitoJUnitRunner.class)
   public class UsersServiceTest {
       
       private static final Logger logger = LoggerFactory.getLogger(UsersServiceTest.class);
   
       @InjectMocks
       private UsersServiceImpl usersService;
   
       @Mock
       private UserMapper userMapper;
       
       @Before
       public void createUser() {
           String userName = "userTest0001";
           String userPassword = "userTest0001";
           User user = new User();
           user.setUserName(userName);
           user.setUserPassword(userPassword);
           userMapper.insert(user);
       }
       
       @Test
       public void testQueryUser() {
           String userName = "userTest0001";
           String userPassword = "userTest0001";
           User queryUser = usersService.queryUser(userName, userPassword);
           logger.info(queryUser.toString());
           Assert.assertTrue(queryUser != null);
       }
   
   }
   ```

