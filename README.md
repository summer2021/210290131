# Apache DolphinScheduler

## Project

API 模块 UT 测试问题解决

## Project progress

- [Project Plan](project-plan.md)
- [MIdterm Report](mid-term.md)
- [Final Report](final.md)

## Member

Mentor: [KejiaChen](https://gitlab.summer-ospp.ac.cn/KejiaChen_210290130)

Student: [jiyuanhao](https://gitlab.summer-ospp.ac.cn/jiyuanhao_210290131)

## DolphinScheduler

- [branch-dolphinscheduler](https://gitlab.summer-ospp.ac.cn/summer2021/210290131/-/tree/dolphinscheduler)

