# 结项报告

## 1. 项目信息

项目名称：API 模块 UT 测试问题解决

项目编号：210290131

项目成员：

- 项目主导师：Kejia Chen
- 学生：Yuanhao Ji

项目仓库：[gitlab.summer-ospp.ac.cn/summer2021/210290131](https://gitlab.summer-ospp.ac.cn/summer2021/210290131)

项目描述：

- 问题：在部分 UT 中使用 PowerMock 后，导致 sonar 不能获取对应的测试报告。
- 大概原因：PowerMock 模拟 JDK 的静态方法、构造方法、final 方法、私有方法时，需要把使用这些方法的类加入到 `@PrepareForTest` 注解中，从而导致单元测试覆盖率不被统计。对此问题进行排查，并给出解决方案。

目前已 merge 的 PR：

- [[Fix-5781][UT] Fix test coverage in sonar](https://github.com/apache/dolphinscheduler/pull/5817)
- [[Improvement-5921][CI] Improve maven connection in CI builds](https://github.com/apache/dolphinscheduler/pull/5924)
- [Add ut template](https://github.com/apache/dolphinscheduler-website/pull/438)

待 merge 的 PR：

- [Add unit test guideline](https://github.com/apache/dolphinscheduler-website/pull/444)

## 2. 方案描述

### 2.1 修复单元测试覆盖率统计问题

1. 问题复现

DolphinScheduer 使用 `PowerMock` 单元测试模拟框架，通过 `JaCoCo` 统计覆盖率指标后由 `Sonar` 存储和管理。

![](img/2.jpg)

因此，为了分析问题出现的原因，首先必须要使问题复现。

在 fork 仓库后，可以在编写测试代码后，通过提交 PR 的方式触发 UT 的工作流，然后检查 `Sonar` 中的覆盖率是否正确。

2. 问题分析

目前，可以猜测这个问题与 `PowerMock` 和 `JaCoCo` 有关，`PowerMock` 模拟 JDK 的静态方法、构造方法、final 方法、私有方法时，需要把使用这些方法的类加入到 `@PrepareForTest` 注解中，从而导致 `JaCoCo` 统计的单元测试覆盖率出现错误。 如以下例子：

```java
public class Demo {
   
    public int method() {
        // do something
        return -1;
    }
   
}
```

```java
@RunWith(PowerMockRunner.class)
@PrepareForTest({Demo.class})
public class DemoTest {

    @Mock
    private Demo demo;

    @Test
    public void testMethod() {
        when(demo.method()).thenReturn(1);
        Assert.assertEquals(1, demo.method());
    }

}
```

在实际测试中，`Demo.class` 的 `method()` 方法的覆盖率为 0

3. 解决方案

`JaCoCo` 的 `offline instrumentation` 可以解决此类问题，它的原理是这样的：在测试前先对 class 文件进行插桩，然后生成插过桩的 class 文件或 jar 包，测试插过桩的 class 和 jar 包后，生成动态覆盖信息到文件，最后统一对覆盖信息进行处理，并生成报告。

`On-the-fly` 模式更方便简单进行代码覆盖分析，无需提前进行字节码插桩，无需考虑 classpath 的设置。

但存在如下情况则不适合 `On-the-fly`，需要采用 `offline` 提前对字节码插桩：

（1）运行环境不支持 java agent。

（2）部署环境不允许设置 JVM 参数。

（3）字节码需要被转换成其他的虚拟机如 Android Dalvik VM。

（4）动态修改字节码过程中和其他 agent 冲突。

（5）无法自定义用户加载类。

而目前问题就是因为动态修改字节码过程中与 `PowerMock` 冲突，因此必须使用 `offline` 模式。

目前已经通过将 `JaCoCo` 切换为 `offline instrumentation` 模式解决了问题，PR: [#5817](https://github.com/apache/dolphinscheduler/pull/5817)

使用 `JaCoCo` `offline instrumentation` 模式替代 `on-the-fly` 模式。`offline instrumentation` 模式可以在测试前先将统计代码（即“插桩”）插入到 class 字节码文件中，基于插过桩的 class 字节码文件进行测试，在测试过程中，对于 `@PrepareForTest` 注解所标记的测试类，`PowerMock` 在加载该类时重新读取的字节码是 `JaCoCo` 修改以后的（在 `on-the-fly` 模式中 `PowerMock` 读取的是原来的字节码，因此会丢失 `JaCoCo` 的修改），所以依然保留了 `JaCoCo` 对其所做的更改，避免了修改冲突，因此 `JaCoCo` 可以正常统计覆盖率相关指标，生成正确的测试报告。

`offline instrumentation` 大致思路：

- 生成插桩的 class 文件（对应插件中的 `instrument`）
- 插过桩的 class 文件进行测试（对应于 `mvn test`）
- 恢复 class 文件（对应插件中的 `restore-instrumented-classes`）
- 生成报告（对应插件中的 `report`）

`JaCoCo` 的 `on-the-fly` 模式和 `offline instrumentation` 模式是通过在 `jacoco-maven-plugin` 插件中执行不同的 `execution` 实现的。`offline instrumentation` 模式的 `jacoco-maven-plugin` 插件配置如下：

```xml
<plugin>
    <groupId>org.jacoco</groupId>
    <artifactId>jacoco-maven-plugin</artifactId>
    <version>${jacoco.version}</version>
    <configuration>
        <dataFile>${project.build.directory}/jacoco.exec</dataFile>
    </configuration>
    <executions>
        <execution>
            <id>default-instrument</id>
            <goals>
                <goal>instrument</goal>
            </goals>
        </execution>
        <execution>
            <id>default-restore-instrumented-classes</id>
            <goals>
                <goal>restore-instrumented-classes</goal>
            </goals>
        </execution>
        <execution>
            <id>default-report</id>
            <goals>
                <goal>report</goal>
            </goals>
        </execution>
    </executions>
</plugin>
```

### 2.2 单元测试模版和编写指导

1. Unit Test Template

> 地址：https://gitlab.summer-ospp.ac.cn/summer2021/210290131/-/blob/master/ut-template/ut-template.md

从单元测试编写原则、编写建议及测试场景角度编写单元测试模版。

其中，单元测试原则重点介绍了 3A 原则和 AIR 原则，并从单元测试设计、质量等方面提出了几点准则。此外，重点针对 Controller 层测试场景和 DAO 层测试场景列举了几点编写建议，并贴出示例以供参考。

- Controller 层测试

  > Controller UT Demo：https://gitlab.summer-ospp.ac.cn/summer2021/210290131/-/tree/master/Test

  Controller UT 架构如下：
  
  ![](img/12.png)

如图，将 Controller 分为两类——NormalController 和 RestController，并分别在其抽象类中定义了公共的测试逻辑。对于新增的 Controller，其测试代码编写步骤如下：

1. 继承 `RestControllerTest` 或 `NormalControllerTest`
2. 重写 `getTestedController()` 方法，返回新增的待测试 controller 对象
3. 基于 `MockMvc` 编写测试方法，模拟 HTTP 请求

其中，`AbstractControllerTest` 类是所有 Controller 测试类的基类，其逻辑如下：

```java
public abstract class AbstractControllerTest {

    protected MockMvc mockMvc;

    @Before
    public void setUp() {
        mockMvc = buildSystemUnderTest();
    }

    protected LocalValidatorFactoryBean validator() {
        return new LocalValidatorFactoryBean();
    }

    protected abstract MockMvc buildSystemUnderTest();

    protected abstract Object getTestedController();

}
```

在由 `@Before` 注解声明的 `setup()` 方法中，通过 `buildSystemUnderTest()` 抽象方法构建 `mockMvc` 对象，并由 `AbstractNormalControllerTest` 和 `AbstractRestControllerTest` 分别重写该方法，实现对不同场景下 `mockMvc` 对象的定制化

`AbstractRestControllerTest ` 逻辑如下：

```java
public abstract class AbstractRestControllerTest extends AbstractControllerTest {

    @Override
    protected MockMvc buildSystemUnderTest() {
        StaticMessageSource messageSource = new StaticMessageSource();
        messageSource.setUseCodeAsDefaultMessage(true);

        MappingJackson2HttpMessageConverter converter = jacksonDateTimeConverter();

        return MockMvcBuilders.standaloneSetup(getTestedController())
                .setHandlerExceptionResolvers(restErrorHandler(messageSource, converter))
                .setMessageConverters(converter)
                .setValidator(validator())
                .build();
    }

    private MappingJackson2HttpMessageConverter jacksonDateTimeConverter() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setObjectMapper(objectMapper);
        return converter;
    }

    private ExceptionHandlerExceptionResolver restErrorHandler(MessageSource messageSource, HttpMessageConverter<?>... messageConverters) {
        ExceptionHandlerExceptionResolver exceptionResolver = new ExceptionHandlerExceptionResolver() {
            @Override
            protected ServletInvocableHandlerMethod getExceptionHandlerMethod(HandlerMethod handlerMethod, Exception exception) {
                Method method = new ExceptionHandlerMethodResolver(TestRestControllerAdvice.class).resolveMethod(exception);
                if (method != null) {
                    return new ServletInvocableHandlerMethod(new TestRestControllerAdvice(), method);
                }
                return super.getExceptionHandlerMethod(handlerMethod, exception);
            }
        };
        exceptionResolver.setMessageConverters(Arrays.asList(messageConverters));
        exceptionResolver.afterPropertiesSet();
        return exceptionResolver;
    }

}
```

在该类中，重写了 `buildSystemUnderTest()` 方法，统一了对于 RestController 中 `mockMvc` 对象的相关设置，如日期转换、异常处理器等，并规定每个具体的 Controller 测试类去实现 `getTestedController()` 方法，返回待测试 Controller 对象，这样设计的目的是：由 Controller 测试类自行构建待测的 Controller 对象，避免从 Spring IoC 容器中获取，从而拜托对 Spring Context 的依赖，加快测试速度

- DAO 层测试

目前 DolphinScheduler DAO 层测试采用了 mock 和基于真实数据库测试两种方式，模版中对这几种方式做了对比，并分别贴出了示例以供参考

DAO 层测试有三种方案：

1. 使用 mock 对象模拟数据库操作
2. 使用内存数据库进行测试
3. 使用真实数据库环境进行测试

三者对比如下：

- 使用模拟对象有利于提高可测试性，避免环境对单元测试的限制。但是这种方案是建立在数据库操作都正确的假想下，因此并不能完全覆盖所有场景（如对 SQL 语句执行结果的测试）。
- 使用内存数据库进行测试同样避免了环境对单元测试的限制，同时使测试更可靠、更快速。
- 使用真实数据库环境进行测试更具有可靠性，但是也对测试环境有了限制，降低了测试的可迁移性，同时数据库操作可能会使测试时间延长，并产生许多脏数据。 

因此，更推荐使用内存数据库进行测试

> 由于 h2 是内存数据库，不会持久化表结构，因此在每次测试前都要先初始化表结构。
> 在 `test/resources` 目录下新建 `application.yml` 文件

```yaml
spring:
  database:
  	driver-class-name: org.h2.Driver
	url: jdbc:h2:mem:test	# test 为数据库名称
    initialization-mode: always	# always: 每次启动时进行初始化
    schema: classpath:sql/schema.sql	# 用于初始化表结构的 sql 文件的路径
    data: classpath:sql/data.sql	# 用于初始化表数据的 sql 文件的路径

# 打印 sql debug 日志
logging:
  level:
    org.apache.dolphinscheduler.dao.mapper: debug
```

使用内存数据库进行测试时，配置好 datasource 后，只需编写业务测试代码即可，无需编写 mock 代码

2. Unit Test Guideline

> 地址：https://gitlab.summer-ospp.ac.cn/summer2021/210290131/-/blob/master/ut-template/guideline.md

该单元测试编写指导的目的是为开发者提供一个可长期使用的单元测试编写思想的指南，考虑到单元测试技术和理论的快速发展，单元测试所使用的框架和示例可能会不断变化，但是单元测试的一些架构设计思想和准则是这一切的基础，是更应该学习和掌握的。

编写指导参考自《Unit Testing Principles, Practices, and Patterns》一书，从单元测试架构、设计风格和反例三个方面进行了全方位的阐述，并贴出示例以便理解。

书中对单元测试风格的讲解非常不错，将单元测试风格分为三种：

- `output-based style`，基于输出结果的测试风格
- `state-based style`，基于状态的测试风格
- `communication-based style`，基于通信的测试风格

其中，`output-based` 测试风格如下图所示，在输入参数之后对业务代码的输出结果进行验证。该测试风格只适用于测试不会改变全局或内部状态的业务代码，因此只需验证其返回值即可。

![](img/14.png)

示例：

`calculateDiscount()` 方法用于计算一组物品的折扣。

```java
public class PriceEngine {
    public double calculateDiscount(Product[] products) {
        double discount = products.length * 0.01;
        return Math.min(discount, 0.02);
    }
}
```

`output-based` 风格的单元测试：

```java
public class PriceEngineTest {
    @Test
    public void calculateDiscount_MinimumDiscount_ReturnMinimumDiscount() {
        // arrange
		Product product1 = new Product("Hand wash");
        Product product2 = new Product("Shampoo");
        PriceEngine priceEngine = new PriceEngine();
        Product[] products = new Product[]{product1, product2};
        // act
        double discount = priceEngine.calculateDiscount(products);
        // assert
        Assert.assertEquals(0.02, discount);
    }
}
```

最终，从防止回归、重构成本、快速反馈、可维护性四个方面对三种测试风格进行了对比，基于 `output-based` 风格的单元测试很少与实现细节耦合在一起，因此重构成本较小。同时由于该风格所具有的简洁、不依赖于外部环境等特性，因此更具有可维护性。

`state-based` 和 `communication-based` 风格的单元测试与实现细节的耦合度更高，因此更难以重构。并且它们代码量往往更大，从而导致更高的维护成本。

因此，更推荐基于 `output-based` 风格编写单元测试。

## 3. 时间规划

|            日期            |                             工作                             |
| :------------------------: | :----------------------------------------------------------: |
|     第一周：7-1 ~ 7-4      | 熟悉 DolphinScheduler API 的 UT，了解 `PowerMock` 和 `Jacoco` |
|   第二、三周：7-5 ~ 7-18   | 搜集资料，理解 `PowerMock` 和 `Jacoco` 的实现原理，复现问题  |
|    第四周：7-19 ~ 7-25     |          定位问题后，编写一份报告描述问题出现的原因          |
|   第五、六周：7-26 ~ 8-8   |                           修复问题                           |
|     第七周：8-9 ~ 8-15     |                修复问题后，编写并提交中期报告                |
|    第八周：8-16 ~ 8-22     |                      总结单元测试的要点                      |
|   第九、十周：8-23 ~ 9-5   |                   编写 Unit Test Template                    |
| 第十一至十三周：9-6 ~ 9-26 |                   编写 Unit Test Guideline                   |
|   第十四周：9-27 ~ 9-30    |            撰写评估文件，提交研发成果进行最终评估            |

## 4. 项目总结

### 4.1 项目产出

已完成的工作：

- 修复 Sonar 中单元测试覆盖率问题
- 完成 Unit Test Template 和 Unit Test Guideline

已 merge 的 PR：

- [[Fix-5781][UT] Fix test coverage in sonar](https://github.com/apache/dolphinscheduler/pull/5817)
- [[Improvement-5921][CI] Improve maven connection in CI builds](https://github.com/apache/dolphinscheduler/pull/5924)
- [Add ut template](https://github.com/apache/dolphinscheduler-website/pull/438)

待 merge 的 PR：

- [Add unit test guideline](https://github.com/apache/dolphinscheduler-website/pull/444)

### 4.2 遇到的问题及解决方案

1. 在修复单元测试覆盖率问题时，尝试使用 `jacoco` 的 `offline` 模式时 `Sonar` 获取测试报告时报错

在执行 `mvn verify sonar:sonar` 时，会首先执行 `jacoco-maven-plugin` 插件，过程中报错信息如下：大意是说 `Sonar` 不能处理修改过的 class 字节码文件，猜测是由于 `JaCoCo` 修改 class 字节码文件后没有恢复造成的。

经过分析，该问题与 Maven 构建生命周期有关。

Maven 构建生命周期定义了一个项目构建、发布的过程。Maven 有以下三个标准的生命周期：

- **clean**：项目清理的处理
- **default(或 build)**：项目部署的处理
- **site**：项目站点文档创建的处理

查看 `jacoco-maven-plugin` 源码得知，`instrument`、`restore-instrumented-classes`、`report` 对应的阶段分别为：`process-classes`、`prepare-package`、`verify`。结合上文得知， `restore-instrumented-classes` 所绑定的 `prepare-package` 阶段在 `test` 之后，而在基于 GitHub Actions 的 ut 工作流中，执行的是 `mvn test`，`restore-instrumented-classes` 并没有执行到，因此也就没有恢复修改的 class 字节码文件。所以，`jacoco-maven-plugin` 至少应该执行 `mvn verify` 才可以执行完全部阶段。

![](img/6.jpg)

### 4.3 项目完成质量

在项目初期，在导师的指导下，快速对问题进行了复现和定位，并深入了解了 `powermock` 和 `jacoco` 分别实现 mock 和统计覆盖率的实现原理，通过将 `jacoco` 设置为 `offline` 模式修复了问题。

在项目的中后期，通过阅读许多单元测试的资料和书籍，对单元测试的相关技术和理论思想有了更深刻的认识，并在此基础上撰写了 Unit Test Template 和 Unit Test Guideline。

## 5. 最后

非常荣幸能够参加本次开源点亮计划，非常感谢 Kejia Chen 导师的热心指导！衷心希望 Apache DolphinScheduler 社区能够越来越好，活动结束后我也会如往常一样参与社区工作，为社区贡献一份力量！

