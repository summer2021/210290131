package com.yuanhaoji.test.controller;

import org.apache.dolphinscheduler.api.utils.Result;
import org.apache.dolphinscheduler.api.enums.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author: shenke
 * @project: Test
 * @date: 2021/9/19
 * @desp:
 */

@RestControllerAdvice
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class MyRestControllerAdvice {

    private static final Logger logger = LoggerFactory.getLogger(MyRestControllerAdvice.class);

    @ExceptionHandler(RuntimeException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Result runtimeException(RuntimeException e) {
        logger.error("RuntimeException: {}", e.getMessage());
        Result result = new Result<>();
        result.setCode(Status.INTERNAL_SERVER_ERROR_ARGS.getCode());
        result.setMsg(e.getMessage());
        return result;
    }

}
