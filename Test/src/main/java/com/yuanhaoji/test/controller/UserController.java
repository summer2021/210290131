package com.yuanhaoji.test.controller;

import com.yuanhaoji.test.service.UserService;
import org.apache.dolphinscheduler.api.controller.BaseController;
import org.apache.dolphinscheduler.api.utils.Result;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author: shenke
 * @project: Test
 * @date: 2021/9/16
 * @desp:
 */
@RestController
public class UserController extends BaseController {

    private final UserService service;

    public UserController(UserService service) {
        this.service = service;
    }

    @RequestMapping("/login")
    @ResponseStatus(HttpStatus.OK)
    public Result login(@RequestParam(value = "userName", required = false) String userName,
                        @RequestParam(value = "userPassword", required = false) String userPassword) {

        Map<String, Object> result = service.login(userName, userPassword);
        return returnDataList(result);
    }

}
