package com.yuanhaoji.test.service;

import org.apache.dolphinscheduler.api.enums.Status;
import org.apache.dolphinscheduler.common.Constants;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: shenke
 * @project: Test
 * @date: 2021/9/16
 * @desp:
 */

@Service
public class UserService {

    public Map<String, Object> login(String userName, String userPassword) {
        Map<String, Object> result = new HashMap<>(16);
        result.put(Constants.STATUS, Status.SUCCESS);
        result.put(Constants.MSG, Status.SUCCESS.getMsg());
        result.put(Constants.DATA_LIST, "");
        return result;
    }

}
