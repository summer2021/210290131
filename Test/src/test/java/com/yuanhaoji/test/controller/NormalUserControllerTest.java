package com.yuanhaoji.test.controller;

import com.yuanhaoji.test.AbstractNormalControllerTest;
import com.yuanhaoji.test.service.UserService;
import org.junit.Test;

/**
 * @author: shenke
 * @project: Test
 * @date: 2021/9/16
 * @desp:
 */
public class NormalUserControllerTest extends AbstractNormalControllerTest {

    @Test
    public void testLogin() {

    }

    @Override
    protected Object getTestedController() {
        UserService service = new UserService();
        return new UserController(service);
    }

}
