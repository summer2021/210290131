package com.yuanhaoji.test.controller;

import com.yuanhaoji.test.AbstractRestControllerTest;
import com.yuanhaoji.test.service.UserService;
import org.apache.dolphinscheduler.api.enums.Status;
import org.apache.dolphinscheduler.api.utils.Result;
import org.apache.dolphinscheduler.common.utils.JSONUtils;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author: shenke
 * @project: Test
 * @date: 2021/9/16
 * @desp:
 */
public class RestUserControllerTest extends AbstractRestControllerTest {

    private static final Logger logger = LoggerFactory.getLogger(RestUserControllerTest.class);

    @Test
    public void testLogin() throws Exception {
        MvcResult mvcResult = mockMvc.perform(post("/login"))
                .andExpect(status().isOk())
                .andReturn();

        MockHttpServletResponse response = mvcResult.getResponse();
        String content = response.getContentAsString();

        Result result = JSONUtils.parseObject(content, Result.class);
        Assert.assertEquals(Status.SUCCESS.getCode(), result.getCode().intValue());
        logger.info(content);
    }

    @Override
    protected Object getTestedController() {
        UserService service = new UserService();
        return new UserController(service);
    }

}
