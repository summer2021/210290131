package com.yuanhaoji.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author: shenke
 * @project: Test
 * @date: 2021/9/19
 * @desp:
 */

@RestControllerAdvice
@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class TestRestControllerAdvice {

    private static final Logger logger = LoggerFactory.getLogger(TestRestControllerAdvice.class);

    @ExceptionHandler(Exception.class)
    public void customException(Exception e) {
        logger.error("Exception: {}", e.getMessage());
    }

    @ExceptionHandler(NullPointerException.class)
    public void nullPointerException(NullPointerException e) {
        logger.error("NullPointerException: {}", e.getMessage());
    }

    @ExceptionHandler(RuntimeException.class)
    public void runtimeException(RuntimeException e) {
        logger.error("RuntimeException: {}", e.getMessage());
    }

}
