package com.yuanhaoji.test;

import org.junit.Before;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

/**
 * @author: shenke
 * @project: Test
 * @date: 2021/9/16
 * @desp:
 */
public abstract class AbstractControllerTest {

    protected MockMvc mockMvc;

    @Before
    public void setUp() {
        mockMvc = buildSystemUnderTest();
    }

    protected LocalValidatorFactoryBean validator() {
        return new LocalValidatorFactoryBean();
    }

    protected abstract MockMvc buildSystemUnderTest();

    protected abstract Object getTestedController();

}
