# Controller 层测试

## 1. 编写建议

1. 为了使测试更加快速，测试时应尽量避免构建 Spring Context。

2. 通过构造函数配置依赖项。

   > 优点：
   >
   > 1. 允许将字段声明为 final。final 关键字会有助于性能提升，并且由于 final 变量是只读的，因此在多线程环境下无需额外的同步开销。
   > 2. 避免通过 Spring 配置依赖项，使测试运行更快。

3. 建议使用 `MockMvc` 对象模拟 HTTP 请求实现 Controller 层测试。

   > 优点：
   >
   > 1. `MockMvc` 可以在不启动 Web 服务器和构建 Spring Context 的情况下实现 Controller 测试。
   > 2. `MockMvc` 提供了许多有用的、用于执行请求和断言结果的方法。

## 2. 编写示例

Controller UT 架构如下：

![](../img/12.png)

对于新增的 controller，其测试代码编写步骤如下：

1. 继承 `RestControllerTest` 或 `NormalControllerTest`
2. 重写 `getTestedController()` 方法，返回新增的待测试 controller 对象
3. 基于 `MockMvc` 编写测试方法，模拟 HTTP 请求

待测试方法：

```java
@RestController
public class UserController extends BaseController {

    private final UserService service;

    public UserController(UserService service) {
        this.service = service;
    }

    @RequestMapping("/login")
    @ResponseStatus(HttpStatus.OK)
    public Result login(@RequestParam(value = "userName", required = false) String userName,
                        @RequestParam(value = "userPassword", required = false) String userPassword) {

        Map<String, Object> result = service.login(userName, userPassword);
        return returnDataList(result);
    }

}
```

测试代码：

```java
public class RestUserControllerTest extends AbstractRestControllerTest {

    private static final Logger logger = LoggerFactory.getLogger(RestUserControllerTest.class);

    @Test
    public void testLogin() throws Exception {
        // (2) Act
        MvcResult mvcResult = mockMvc.perform(post("/login"))
                .andExpect(status().isOk())    // (3) Assert
                .andReturn();

        MockHttpServletResponse response = mvcResult.getResponse();
        String content = response.getContentAsString();

        Result result = JSONUtils.parseObject(content, Result.class);
        // (3) Assert
        Assert.assertEquals(Status.SUCCESS.getCode(), result.getCode().intValue());
        logger.info(content);
    }

    @Override
    protected Object getTestedController() {
        // (1) Arrange
        UserService service = new UserService();
        return new UserController(service);
    }

}
```
